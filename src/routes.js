import Home from './components/marketing/Home'
import Login from './components/auth/Login'
import Logout from './components/auth/Logout'
import Dashboard from './components/app/Dashboard'
import Patients from './components/app/Patients'
import Orders from './components/app/Orders'
import Users from './components/app/Users'
import Settings from './components/app/Settings'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: Login
    // props: true,
    // meta: {
    //     requiresVisitor: true,
    // }
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard
    // meta: {
    //   requiresAuth: true,
    // }
  },
  {
    path: '/patients',
    name: 'patients',
    component: Patients
  },
  {
    path: '/orders',
    name: 'orders',
    component: Orders
  },
  {
    path: '/users',
    name: 'users',
    component: Users
  },
  {
    path: '/settings',
    name: 'settings',
    component: Settings
  },
  {
    path: '/logout',
    name: 'logout',
    component: Logout
  }
]

export default routes
