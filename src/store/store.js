import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = 'http://192.168.10.10/api'

export const store = new Vuex.Store({
  state: {
    token: localStorage.getItem('access_token') || null,
    users: [],
    account: [],
    rewards: [],
    apts: [],
    orders: []
  },
  /////
  getters: {
    loggedIn(state) {
      return state.token !== null
    },
    getUsers(state) {
      return state.users
    },
    getRewards(state) {
      return state.rewards
    },
    getApts(state) {
      return state.apts
    },
    getOrders(state) {
      return state.orders
    }
  },
  /////
  mutations: {
    /// Login
    retrieveToken(state, token) {
      state.token = token
    },
    destroyToken(state) {
      state.token = null
    },
    /// Account
    updateAccount(state, account) {
      state.account = account
    },
    /// Rewards
    getRewards(state, rewards) {
      state.rewards = rewards
    },
    addReward(state, reward) {
      const index = state.rewards.findIndex(item => item.id == reward.id)
      state.rewards.splice(index, 1, {
        id: reward.id,
        name: reward.name,
        value: reward.value,
        points: reward.points,
        active: reward.active
      })
    },
    updateReward(state, reward) {
      const index = state.rewards.findIndex(item => item.id == reward.id)
      state.rewards.splice(index, 1, {
        id: reward.id,
        name: reward.name,
        value: reward.value,
        points: reward.points,
        active: reward.active
      })
    },
    deleteReward(state, id) {
      const index = state.rewards.findIndex(item => item.id == id)
      state.rewards.splice(index, 1)
    },
    /// Appointments
    getApts(state, apts) {
      state.apts = apts
    },
    addApt(state, apt) {
      const index = state.apts.findIndex(item => item.id == apt.id)
      state.apts.splice(index, 1, {
        id: apt.id,
        name: apt.name,
        points: apt.points,
        active: apt.active
      })
    },
    updateApt(state, apt) {
      const index = state.apts.findIndex(item => item.id == apt.id)
      state.apts.splice(index, 1, {
        id: apt.id,
        name: apt.name,
        points: apt.points,
        active: apt.active
      })
    },
    deleteApt(state, id) {
      const index = state.apts.findIndex(item => item.id == id)
      state.apts.splice(index, 1)
    },
    /// Users
    getUsers(state, users) {
      state.users = users
    },
    updateUser(state, user) {
      const index = state.users.findIndex(item => item.id == user.id)
      state.users.splice(index, 1, {
        id: user.id,
        name: user.name,
        email: user.email,
        role: user.role,
        active: user.active
      })
    },
    deleteUser(state, id) {
      const index = state.users.findIndex(item => item.id == id)
      state.users.splice(index, 1)
    },
    // Orders
    getOrders(state, orders) {
      state.orders = orders
    }
  },
  /////
  actions: {
    //Login
    retrieveToken(context, credentials) {
      return new Promise((resolve, reject) => {
        axios
          .post('/login', {
            email: credentials.email,
            password: credentials.password
          })
          .then(response => {
            const token = response.data.access_token
            localStorage.setItem('access_token', token)
            context.commit('retrieveToken', token)
            resolve(response)
          })
          .catch(error => {
            //   console.log(error)
            reject(error)
          })
      })
    },
    destroyToken(context) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token

      if (context.getters.loggedIn) {
        return new Promise((resolve, reject) => {
          axios
            .post('/logout')
            .then(response => {
              localStorage.removeItem('access_token')
              context.commit('destroyToken')
              resolve(response)
            })
            .catch(error => {
              localStorage.removeItem('access_token')
              context.commit('destroyToken')
              reject(error)
            })
        })
      }
    },
    //Accounts
    getAccount(context) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token

      return new Promise((resolve, reject) => {
        axios
          .get('/account')
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    updateAccount(context, account) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token

      return new Promise((resolve, reject) => {
        axios
          .patch('/account/' + account.id, {
            id: account.id,
            company_name: account.company_name,
            phone: account.phone,
            email: account.email,
            address: account.address,
            city_prov: account.city_prov,
            country: account.country,
            postal_code: account.postal_code
          })
          .then(response => {
            context.commit('updateAccount', response.data)
            resolve(response)
          })
          .catch(error => {
            reject(error)
            console.log(error)
          })
      })
    },
    //Rewards
    getRewards(context) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      // return new Promise((resolve, reject) => {
      axios
        .get('/rewards')
        .then(response => {
          context.commit('getRewards', response.data)
          // resolve(response)
        })
        .catch(error => {
          console.log(error)
          // reject(error)
        })
      // })
    },
    addReward(context, reward) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      axios
        .post('/rewards', {
          name: reward.name,
          value: reward.value,
          points: reward.points,
          active: reward.active
        })
        .then(response => {
          context.commit('addReward', response.data)
        })
        .catch(error => {
          console.log(error)
        })
    },
    updateReward(context, reward) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      axios
        .patch('/rewards/' + reward.id, {
          id: reward.id,
          name: reward.name,
          value: reward.value,
          points: reward.points,
          active: reward.active
        })
        .then(response => {
          context.commit('updateReward', response.data)
        })
        .catch(error => {
          console.log(error)
        })
    },
    deleteReward(context, id) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      return new Promise((resolve, reject) => {
        axios
          .delete('/rewards/' + id)
          .then(response => {
            context.commit('deleteReward', id)
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    //Apts Type
    getApts(context) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      axios
        .get('/apts')
        .then(response => {
          context.commit('getApts', response.data)
        })
        .catch(error => {
          console.log(error)
        })
    },
    addApt(context, apt) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      axios
        .post('/apt', {
          name: apt.name,
          points: apt.points,
          active: apt.active
        })
        .then(response => {
          context.commit('addApt', response.data)
        })
        .catch(error => {
          console.log(error)
        })
    },
    updateApt(context, apt) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      axios
        .patch('/apt/' + apt.id, {
          id: apt.id,
          name: apt.name,
          points: apt.points,
          active: apt.active
        })
        .then(response => {
          context.commit('updateApt', response.data)
        })
        .catch(error => {
          console.log(error)
        })
    },
    deleteApt(context, id) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      return new Promise((resolve, reject) => {
        axios
          .delete('/apt/' + id)
          .then(response => {
            context.commit('deleteApt', id)
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    //Users
    getUsers(context) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token

      axios
        .get('/users')
        .then(response => {
          context.commit('getUsers', response.data)
        })
        .catch(error => {
          console.log(error)
        })
    },
    addUser(context, user) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      return new Promise((resolve, reject) => {
        axios
          .post('/user', {
            name: user.name,
            role: user.role,
            email: user.email,
            password: user.password,
            active: user.active
          })
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    updateUser(context, user) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      axios
        .patch('/user/' + user.id, {
          id: user.id,
          name: user.name,
          email: user.email,
          role: user.role,
          active: user.active
        })
        .then(response => {
          context.commit('updateUser', response.data)
        })
        .catch(error => {
          console.log(error)
        })
    },
    deleteUser(context, id) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      return new Promise((resolve, reject) => {
        axios
          .delete('/user/' + id)
          .then(response => {
            context.commit('deleteUser', id)
            resolve(response)
          })
          .catch(error => {
            reject(error)
          })
      })
    },
    // Orders
    getOrders(context) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token

      axios
        .get('/orders')
        .then(response => {
          context.commit('getOrders', response.data)
        })
        .catch(error => {
          console.log(error)
        })
    },
    updateOrder(context, order) {
      axios.defaults.headers.common['Authorization'] =
        'Bearer ' + context.state.token
      return new Promise((resolve, reject) => {
        axios
          .patch('/order/' + order.id, {
            id: order.id,
            action: order.action,
            reason: order.reason
          })
          .then(response => {
            resolve(response)
            // context.commit('closeOrder', response.data)
          })
          .catch(error => {
            reject(error)
          })
      })
    }
  }
})
