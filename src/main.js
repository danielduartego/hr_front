import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './routes'
import Vuex from 'vuex'
import BootstrapVue from 'bootstrap-vue'
import ToggleButton from 'vue-js-toggle-button'

Vue.use(ToggleButton)
Vue.use(BootstrapVue)
Vue.use(require('vue-moment'))

//components
import Master from './Master.vue'
import { store } from './store/store'

window.eventBus = new Vue()
Vue.config.productionTip = false

//use
Vue.use(VueRouter)
Vue.use(Vuex)

//routes
const router = new VueRouter({
  routes,
  mode: 'history'
})

//render
new Vue({
  router: router,
  store: store,
  render: h => h(Master)
}).$mount('#app')
